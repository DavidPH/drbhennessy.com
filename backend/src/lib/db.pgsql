CREATE TABLE IF NOT EXISTS "users" (
    user_id INTEGER UNIQUE GENERATED ALWAYS AS IDENTITY,
    user_name TEXT UNIQUE NOT NULL,
    user_email TEXT UNIQUE NOT NULL,
    password TEXT,
    salt TEXT,
    permission_level INTEGER DEFAULT 0,
  	PRIMARY KEY (user_id)
);

CREATE TABLE IF NOT EXISTS "categories" (
    category_id INTEGER UNIQUE GENERATED ALWAYS AS IDENTITY,
    category_name_en TEXT UNIQUE NOT NULL,
    category_name_es TEXT UNIQUE NOT NULL,
    CONSTRAINT name_not_empty
        CHECK (
            ( ((coalesce(TRIM(category_name_en), '') <> '') AND (coalesce(TRIM(category_name_es), '') <> '')) )
        )
);

CREATE TABLE IF NOT EXISTS "text_posts" (
    post_id INTEGER UNIQUE GENERATED ALWAYS AS IDENTITY,
    category_id INTEGER NOT NULL,
    author INTEGER NOT NULL,
    created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP,
    title_en TEXT,
    title_es TEXT,
    body_en TEXT,
    body_es TEXT,
  	CONSTRAINT if_title_or_body_empty
   		CHECK ( 
            ( (coalesce(TRIM(title_en), '') <> '') AND (coalesce(TRIM(body_en), '') <> '') ) OR
            ( (coalesce(TRIM(title_es), '') <> '') AND (coalesce(TRIM(body_es), '') <> '') )
        ),
    PRIMARY KEY (post_id),
    FOREIGN KEY (author)
        REFERENCES "users" (user_id)
        ON UPDATE CASCADE,
    FOREIGN KEY (category_id)
        REFERENCES "categories" (category_id)
        ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS "audio_posts" (
    post_id INTEGER UNIQUE GENERATED ALWAYS AS IDENTITY,
    author INTEGER NOT NULL,
    created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP,
    title TEXT NOT NULL,
    description TEXT,
    media_uri TEXT NOT NULL,
    PRIMARY KEY (post_id),
    FOREIGN KEY (author)
        REFERENCES "users" (user_id)
        ON UPDATE CASCADE
)
