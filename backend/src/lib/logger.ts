import { inspect } from "util";
import winston from "winston";

const isDev = process.env.NODE_ENV === "dev";

const { combine, timestamp, printf, colorize } = winston.format;

const customFormat = printf(({ level, message, timestamp, stack }) => {
	if (typeof message === "object") {
		message = inspect(message);
	}
	return `[${timestamp}][${level}]: ${stack ?? message}`;
});

const logger = winston.createLogger({
	level: isDev ? "debug" : "info",
	transports: [new winston.transports.Console({ format: combine(colorize(), timestamp(), customFormat) })]
});

if (!isDev) {
	logger.add(new winston.transports.File({ filename: "error.log", level: "error", format: combine(timestamp(), customFormat) }));
	logger.add(new winston.transports.File({ filename: "combined.log", format: combine(timestamp(), customFormat) }));
}

export default logger;
