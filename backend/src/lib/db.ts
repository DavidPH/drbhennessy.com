import postgres from "postgres";
import logger from "./logger";

const db = postgres(process.env.DB_URL as string, {
	onnotice: (n) => {
		// Don't care about "relation ___ already exists, skipping" notice
		if (n.code !== "42P07") {
			logger.warn(n);
		}
	}
});

export async function initDB() {
	// TODO: Fix this path since /src/ won't exist in production.
	await db.file("src/lib/db.pgsql");
}

export default db;
