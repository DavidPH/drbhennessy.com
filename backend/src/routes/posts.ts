import { s, Result, InferType } from "@sapphire/shapeshift";
import express from "express";
import moment from "moment";
import logger from "#lib/logger";
import db from "#lib/db";
import { PostgresError } from "postgres";
const router = express.Router();

const textPostValidation = s.object({
	category_id: s.number.greaterThanOrEqual(0),
	author: s.number,
	created_at: s.string.optional.reshape((value) => {
		const m = moment(value, moment.ISO_8601, true);
		return m.isValid() ? Result.ok(m.toISOString()) : Result.err(new Error("Invalid Date"));
	}),
	title_en: s.string.lengthNotEqual(0).or(s.null),
	title_es: s.string.lengthNotEqual(0).or(s.null),
	body_en: s.string.lengthNotEqual(0).or(s.null),
	body_es: s.string.lengthNotEqual(0).or(s.null)
}).strict;

interface textPost extends Omit<InferType<typeof textPostValidation>, "created_at"> {
	post_id: number;
	created_at: Date;
}

const getQueryVAlidation = s.object({
	lang: s.literal("en").or(s.literal("es")).optional,
	categoryID: s.union(s.string, s.string.array).optional.reshape((value) => {
		if (value === undefined) return Result.ok(value);
		const error = new Error("Invalid Category ID");
		if (typeof value === "string") {
			const n = parseInt(value, 10);
			return isNaN(n) ? Result.err(error) : Result.ok(n);
		}
		const values = value.map((v) => parseInt(v, 10));
		return values.some(isNaN) ? Result.err(error) : Result.ok(values);
	}),
	before: s.string.reshape((value) => {
		const m = moment(value, moment.ISO_8601, true);
		return m.isValid() ? Result.ok(m.toISOString()) : Result.err(new Error("Invalid Date"));
	}).optional,
	after: s.string.reshape((value) => {
		const m = moment(value, moment.ISO_8601, true);
		return m.isValid() ? Result.ok(m.toISOString()) : Result.err(new Error("Invalid Date"));
	}).optional
});

/*
	GET /api/posts/
	params:
		- lang: en | es
		- before: ISO timestamp
		- after: ISO timestamp
		- categoryID: number
*/
router.get("/", async (req, res) => {
	try {
		const query = getQueryVAlidation.parse(req.query);
		try {
			const result = await db<textPost[]>`SELECT * FROM text_posts ${
				Object.keys(query).length > 0
					? db`
						WHERE 
						${query.lang === undefined ? db`` : db`${db(`title_${query.lang}`)} is not null AND `}
						${query.after === undefined ? db`` : db`created_at >= ${query.after} AND `}
						${query.before === undefined ? db`` : db`created_at < ${query.before} AND `}
						${query.categoryID === undefined ? db`` : db`category_id = ANY(${db.array([query.categoryID])}::int[]) AND `}
						TRUE` // To close out any hanging 'AND'
					: db``
			}
			`;
			res.contentType("json").send(result);
			logger.debug(result);
		} catch (e) {
			if (e instanceof Error) {
				logger.error("", e);
				return res.status(500).send();
			}
		}
	} catch {
		return res.status(400).send("Invalid query params.");
	}
});

// GET /api/posts/{id}
router.get("/:id", async (req, res) => {
	const result = await db<textPost[]>`SELECT * FROM text_posts WHERE post_id = ${req.params.id}`;
	logger.debug(result);
	if (result.length === 0) return res.status(404).send(`Text Post with ID of ${req.params.id} not found.`);
	res.contentType("json").send(result[0]);
});

// POST /api/posts
router.post("/", express.json(), async (req, res) => {
	try {
		const post = textPostValidation.parse(req.body);
		try {
			res.contentType("json");
			if (post.created_at !== undefined) {
				res.send(
					await db`INSERT INTO text_posts ${db(
						post as InferType<typeof textPostValidation.required>,
						"created_at",
						"category_id",
						"author",
						"title_en",
						"title_es",
						"body_en",
						"body_es"
					)}
				RETURNING *`
				);
			} else {
				res.send(
					await db`INSERT INTO text_posts ${db(post, "category_id", "author", "title_en", "title_es", "body_en", "body_es")} RETURNING *`
				);
			}
		} catch (e) {
			if (e instanceof PostgresError) {
				return res.status(400).send(e.message);
			} else if (e instanceof Error) {
				logger.error("", e);
				return res.status(500).send();
			}
		}
	} catch (e) {
		return res.status(400).send("Invalid JSON object.");
	}
});

// PUT /api/posts/{id}
router.put("/:id", express.json(), async (req, res) => {
	try {
		const post = textPostValidation.parse(req.body);
		try {
			const [{ exists }] = await db<{ exists: boolean }[]>`
			SELECT EXISTS(SELECT 1 from text_posts WHERE post_id = ${req.params.id}) AS "exists"`;
			if (!exists) return res.status(404).send(`Text Post with ID of ${req.params.id} not found`);
			res.contentType("json");
			if (post.created_at !== undefined) {
				res.send(
					await db`UPDATE text_posts set ${db(
						post as InferType<typeof textPostValidation.required>,
						"created_at",
						"category_id",
						"author",
						"title_en",
						"title_es",
						"body_en",
						"body_es"
					)} WHERE post_id = ${req.params.id}
				RETURNING *`
				);
			} else {
				res.send(
					await db`UPDATE text_posts set ${db(
						post,
						"category_id",
						"author",
						"title_en",
						"title_es",
						"body_en",
						"body_es"
					)} WHERE post_id = ${req.params.id} RETURNING *`
				);
			}
		} catch (e) {
			if (e instanceof PostgresError) {
				return res.status(400).send(e.message);
			} else if (e instanceof Error) {
				logger.error("", e);
				return res.status(500).send();
			}
		}
	} catch (e) {
		return res.status(400).send("Invalid JSON object.");
	}
});

// DELETE /api/posts/{id}
router.delete("/:id", async (req, res) => {
	const id = parseInt(req.params.id, 10);
	if (isNaN(id)) return res.status(400).send("Invalid ID");
	// TODO put posts in a 'recycle bin'
	const result = await db`DELETE FROM text_posts WHERE post_id = ${id}`;
	if (result.count === 0) return res.status(404).send(`Text Post with ID of ${id} not found`);
	res.status(200).send();
});

export default router;
