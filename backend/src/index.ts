import express, { NextFunction, Response, Request } from "express";
import path from "path";
import fs from "fs";
import { initDB } from "#lib/db";
import logger from "#lib/logger";

const PORT = 5000;
const app = express();

// Registers routes under /api/filename
async function registerRoutes() {
	const routePath = path.join(__dirname, "routes");
	const files = fs.readdirSync(routePath);
	for await (const file of files) {
		if (!file.endsWith(".js")) continue;
		const route = path.join(routePath, file);
		try {
			const item = await import(route);
			app.use(`/api/${file.slice(0, -3)}`, item.default);
		} catch (error) {
			if (error instanceof Error) {
				logger.error(`Error loading route ${route}`, error.message);
			}
		}
	}

	// Error handling
	app.use((err: unknown, req: Request, res: Response, next: NextFunction) => {
		if (err instanceof SyntaxError) {
			return res.status(400).send(err.message);
		}
		next(err);
	});
}

// Checks for read and write access in the media volume
fs.access("/opt/media", fs.constants.R_OK | fs.constants.W_OK, async (error) => {
	if (error) {
		logger.error(error.message);
	} else {
		try {
			await initDB();
			await registerRoutes();
			app.listen(PORT, () => logger.info(`Listening on port ${PORT}`));
		} catch (e) {
			if (e instanceof Error) {
				logger.error(e.message);
			}
		}
	}
});
